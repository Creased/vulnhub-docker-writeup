# Nmap 7.70 scan initiated Fri Apr 13 10:17:14 2018 as: nmap -sV -sC -O -T5 -p- -oN nmap/discover.txt docker
Nmap scan report for docker (192.168.56.104)
Host is up (0.00057s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 6.6p1 Ubuntu 2ubuntu1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 45:13:08:81:70:6d:46:c3:50:ed:3c:ab:ae:d6:e1:85 (DSA)
|   2048 4c:e7:2b:01:52:16:1d:5c:6b:09:9d:3d:4b:bb:79:90 (RSA)
|   256 cc:2f:62:71:4c:ea:6c:a6:d8:a7:4f:eb:82:2a:22:ba (ECDSA)
|_  256 73:bf:b4:d6:ad:51:e3:99:26:29:b7:42:e3:ff:c3:81 (ED25519)
2375/tcp open  docker  Docker 17.06.0-ce
| docker-version: 
|   KernelVersion: 3.13.0-128-generic
|   BuildTime: 2017-06-23T21:17:13.228983331+00:00
|   GitCommit: 02c1d87
|   Version: 17.06.0-ce
|   GoVersion: go1.8.3
|   Os: linux
|   Arch: amd64
|   MinAPIVersion: 1.12
|_  ApiVersion: 1.30
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.0 404 Not Found
|     Content-Type: application/json
|     Date: Fri, 13 Apr 2018 08:17:46 GMT
|     Content-Length: 29
|     {"message":"page not found"}
|   GenericLines, Help, Kerberos, LPDString, RTSPRequest, SSLSessionReq, TLSSessionReq: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 404 Not Found
|     Content-Type: application/json
|     Date: Fri, 13 Apr 2018 08:17:21 GMT
|     Content-Length: 29
|     {"message":"page not found"}
|   HTTPOptions: 
|     HTTP/1.0 200 OK
|     Api-Version: 1.30
|     Docker-Experimental: false
|     Ostype: linux
|     Server: Docker/17.06.0-ce (linux)
|     Date: Fri, 13 Apr 2018 08:17:21 GMT
|     Content-Length: 0
|     Content-Type: text/plain; charset=utf-8
|   docker: 
|     HTTP/1.1 400 Bad Request: missing required Host header
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|_    Request: missing required Host header
8000/tcp open  http    Apache httpd 2.4.10 ((Debian))
|_http-generator: WordPress 4.8.1
|_http-open-proxy: Proxy might be redirecting requests
| http-robots.txt: 1 disallowed entry 
|_/wp-admin/
|_http-server-header: Apache/2.4.10 (Debian)
|_http-title: NotSoEasy Docker &#8211; Just another WordPress site
|_http-trane-info: Problem with XML parsing of /evox/about
MAC Address: 08:00:27:D9:C7:82 (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 3.X|4.X
OS CPE: cpe:/o:linux:linux_kernel:3 cpe:/o:linux:linux_kernel:4
OS details: Linux 3.2 - 4.9
Network Distance: 1 hop
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Apr 13 10:18:49 2018 -- 1 IP address (1 host up) scanned in 96.08 seconds
